// @Title 名称
// @Description 描述
// @Author Rachel
// @CreateTime 2022/1/28 10:47 上午

package entity

type PageHelper struct {
	Page     int `json:"page"`
	PageSize int `json:"page_size"`
}

func (pageHelper PageHelper) CurrentPage() int {
	if pageHelper.Page == 0 {
		pageHelper.Page = 1
	}

	if pageHelper.PageSize == 0 {
		pageHelper.PageSize = 10
	}

	return (pageHelper.Page - 1) * pageHelper.PageSize
}
