package entity

import (
	"encoding/json"
	"gitee.com/kynchen/rachel_golang_common/common/constants"
)

type ResultBuild struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data"`
}

// ResultSuccess 统一返回类
func (resultBuild ResultBuild) ResultSuccess() []byte {
	resultBuild.Code = constants.SUCCESS
	resultBuild.Msg = constants.SuccessMsg
	resultBuild.Data = nil
	marshal, _ := json.Marshal(resultBuild)
	return marshal
}

// ResultSuccessInstance 统一返回类
func (resultBuild ResultBuild) ResultSuccessInstance() ResultBuild {
	resultBuild.Code = constants.SUCCESS
	resultBuild.Msg = constants.SuccessMsg
	resultBuild.Data = nil
	return resultBuild
}

// ResultSuccessData  统一返回类
func (resultBuild ResultBuild) ResultSuccessData(data interface{}) []byte {
	resultBuild.Code = constants.SUCCESS
	resultBuild.Msg = constants.SuccessMsg
	resultBuild.Data = data
	marshal, _ := json.Marshal(resultBuild)
	return marshal
}

// ResultError 返回系统异常
func (resultBuild ResultBuild) ResultError() []byte {
	resultBuild.Code = constants.SystemError
	resultBuild.Msg = constants.SystemErrorMsg
	resultBuild.Data = nil
	marshal, _ := json.Marshal(resultBuild)
	return marshal
}

// ResultErrorInstance 返回系统异常
func (resultBuild ResultBuild) ResultErrorInstance() ResultBuild {
	resultBuild.Code = constants.SystemError
	resultBuild.Msg = constants.SystemErrorMsg
	resultBuild.Data = nil
	return resultBuild
}

// ResultErrorMsg
// code 状态码，msg 消息
func (resultBuild ResultBuild) ResultErrorMsg(code int, msg string) []byte {
	resultBuild.Code = code
	resultBuild.Msg = msg
	resultBuild.Data = nil
	marshal, _ := json.Marshal(resultBuild)
	return marshal
}

func (resultBuild ResultBuild) MarshalBinary() []byte {
	result, _ := json.Marshal(resultBuild)
	return result
}
