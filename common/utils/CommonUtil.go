// @Title 名称
// @Description 描述
// @Author Rachel
// @CreateTime 2022/1/29 8:14 下午

package utils

import (
	"errors"
	"reflect"
)

// Contains 判断是否包含
func Contains(obj interface{}, target interface{}) (bool, error) {
	targetValue := reflect.ValueOf(target)
	switch reflect.TypeOf(target).Kind() {
	case reflect.Slice, reflect.Array:
		for i := 0; i < targetValue.Len(); i++ {
			if targetValue.Index(i).Interface() == obj {
				return true, nil
			}
		}
	case reflect.Map:
		if targetValue.MapIndex(reflect.ValueOf(obj)).IsValid() {
			return true, nil
		}
	}

	return false, errors.New("not in array")
}

// IsEmpty 判断对象是否为空
func IsEmpty(in interface{}) bool {

	return in == nil || reflect.ValueOf(in).IsZero()
}